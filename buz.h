/*
 * buz.h
 *
 *  Created on: 16 Oct 2016
 *      Author: Dominic
 */

#ifndef BUZ_H_
#define BUZ_H_

#define LOWP 4999
#define HIGHP  1666

void buz_init();
void buz_buzbuz(int buzzes, int pitch);
#endif /* BUZ_H_ */
