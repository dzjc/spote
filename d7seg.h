/*
 * 7seg.h
 *
 *  Created on: 25 Sep 2016
 *      Author: Dominic
 */

#ifndef D7SEG_H_
#define D7SEG_H_

void d7seg_init();
void d7seg_printNum(int num);
void d7seg_printMode(int num);
void d7seg_printStr(char* msg);

#endif /* 7SEG_H_ */
