/*
 * adc.c
 *
 *  Created on: 29 Sep 2016
 *      Author: Dominic
 */
#include <stdint.h>
#include "msp430fr5739.h"
#include "driverlib/driverlib.h"
#include "d7seg.h"

#define BUFFER_SIZE 20
int16_t buffer[BUFFER_SIZE] = {0};
int headIndex = 0;
int tailIndex = 0;

int16_t filtered = 0;
int16_t sum = 0;

#pragma PERSISTENT(zero_offset)
int16_t zero_offset = 78;
#pragma PERSISTENT(scale_factor)
float scale_factor = 5.0;

void adc_setZero(){
	int16_t temp = 0;
	temp = (int16_t)ADC10_B_getResults(ADC10_B_BASE);
	zero_offset = temp;
}

void adc_setScale(){
	int16_t temp = 0;
	temp = (int16_t)ADC10_B_getResults(ADC10_B_BASE);
	scale_factor = 3000.0/temp;
}

/*
 * Input data filtering, should be called regularly
 */
int16_t adc_read(){
	//catch buffer underflows
	if(tailIndex == headIndex){
		//int i = 0;
	}else{
		sum = buffer[tailIndex] + buffer[(tailIndex + 1) % BUFFER_SIZE] + buffer[(tailIndex + 2) % BUFFER_SIZE] + buffer[(tailIndex + 3) % BUFFER_SIZE];
		sum = sum >> 2;
		filtered = buffer[tailIndex];
		tailIndex+= 1;
		if(tailIndex == BUFFER_SIZE){
			tailIndex = 0;
		}
	}
	return (int16_t) filtered * scale_factor;
}

void adc_init(){
	P2SEL1 |= BIT3 | BIT4;
	P2SEL0 |= BIT3 | BIT4;

	//initialise buffer to 0;
	int i =0;
	for(i=0; i < BUFFER_SIZE; i++){
		buffer[i]=0;
	}

    //Configure internal voltage reference
    //Wait while refgen is busy. Busy wait (probs) ok since it only runs at init time.
    while(REF_BUSY == Ref_isRefGenBusy(REF_BASE));
    Ref_setReferenceVoltage(REF_BASE, REF_VREF2_5V); //set internal ref to 2.5V
    Ref_enableTempSensor(REF_BASE); //TODO - disable temp sensor in production code...
    Ref_enableReferenceVoltage(REF_BASE);

	Timer_A_initUpModeParam param = {0};
	param.clockSource = TIMER_A_CLOCKSOURCE_SMCLK;
	param.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_20;
	param.timerInterruptEnable_TAIE = TIMER_A_TAIE_INTERRUPT_DISABLE;
	param.captureCompareInterruptEnable_CCR0_CCIE = TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE;
	param.timerClear = TIMER_A_DO_CLEAR;
	param.startTimer = false;
	param.timerPeriod = 39; //Interrupt freq of 10kHz

	Timer_A_initUpMode(TIMER_A1_BASE, &param);

    //Configure ADC, ADC10 built in oscillator
    ADC10_B_init(ADC10_B_BASE,
            ADC10_B_SAMPLEHOLDSOURCE_SC,
            ADC10_B_CLOCKSOURCE_ADC10OSC,
            ADC10_B_CLOCKDIVIDER_1);


    ADC10_B_enableInterrupt(ADC10_B_BASE,
            ADC10_B_IE0); //enable interrupt for new conversion data in memory buffer

    ADC10_B_enable(ADC10_B_BASE);

    ADC10_B_configureMemory(ADC10_B_BASE,
            ADC10_B_INPUT_A6,      //
            ADC10_B_VREFPOS_INT,   //2.5V internal reference        
            ADC10_B_VREFNEG_AVSS);
    ADC10_B_setupSamplingTimer(ADC10_B_BASE, ADC10_B_CYCLEHOLD_512_CYCLES, ADC10_B_MULTIPLESAMPLESDISABLE);

    while(ADC10_B_BUSY == ADC10_B_isBusy(ADC10_B_BASE));
    ADC10_B_startConversion(ADC10_B_BASE,
    		ADC10_B_REPEATED_SINGLECHANNEL); //Continuous conversion

	Timer_A_startCounter(TIMER_A1_BASE, TIMER_A_UP_MODE);
}

#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void){
	ADC10_B_clearInterrupt(ADC10_B_BASE, ADC10_B_IFG0);
	buffer[headIndex++] = ((int16_t)ADC10_B_getResults(ADC10_B_BASE)) - zero_offset;
    if(headIndex == BUFFER_SIZE){
    	headIndex = 0;
    }
    if(headIndex == tailIndex){
    	//BUFFER OVERFLOW~~~ :(
    	//overwrite last entry
    	headIndex = (headIndex == 0) ? BUFFER_SIZE - 1 : headIndex - 1;
    }
}

#pragma vector=TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR(void){
	ADC10CTL0 ^= ADC10SC;
}
