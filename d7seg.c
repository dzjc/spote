/*
 * d7seg.c
 *
 *  Created on: 25 Sep 2016
 *      Author: Dominic
 */

#include "d7seg.h"
#include <msp430fr5739.h>
#include "driverlib/driverlib.h"

#define SEG_A BIT0
#define SEG_B BIT1
#define SEG_C BIT2
#define SEG_D BIT3
#define SEG_E BIT4
#define SEG_F BIT5
#define SEG_G BIT6
#define SEG_DP BIT7

uint8_t numbers[10] = {
	SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F , //0
	SEG_B|SEG_C, //1
	SEG_A|SEG_B|SEG_D|SEG_E|SEG_G, //2
	SEG_A|SEG_B|SEG_C|SEG_D|SEG_G, //3
	SEG_B|SEG_C|SEG_F|SEG_G, //4
	SEG_A|SEG_C|SEG_D|SEG_F|SEG_G, //5
	SEG_A|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G, //6
	SEG_A|SEG_B|SEG_C, //7
	SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G, //8
	SEG_A|SEG_B|SEG_C|SEG_D|SEG_F|SEG_G, //9
};

uint8_t alphabet[26] = {
	SEG_A|SEG_B|SEG_C|SEG_E|SEG_F|SEG_G, //A
	SEG_C|SEG_D|SEG_E|SEG_F|SEG_G, //b
	SEG_A|SEG_D|SEG_E|SEG_F, //C
	SEG_B|SEG_C|SEG_D|SEG_E|SEG_G, //d
	SEG_A|SEG_D|SEG_E|SEG_F|SEG_G, //E
	SEG_A|SEG_E|SEG_F|SEG_G, //F
	SEG_A|SEG_B|SEG_C|SEG_D|SEG_F|SEG_G, //g
	SEG_C|SEG_E|SEG_F|SEG_G, //h
	SEG_B|SEG_C,//I
	SEG_B|SEG_C|SEG_D|SEG_E,//J
	SEG_B|SEG_C|SEG_E|SEG_F|SEG_G, //K
	SEG_D|SEG_E|SEG_F, //L
	SEG_A|SEG_B|SEG_C|SEG_E|SEG_F, //M
	SEG_C|SEG_E|SEG_G, //n
	SEG_C|SEG_D|SEG_E|SEG_G, //o
	SEG_A|SEG_B|SEG_E|SEG_F|SEG_G, //p
	SEG_A|SEG_B|SEG_C|SEG_F|SEG_G, //q
	SEG_E|SEG_G, //r
	SEG_A|SEG_C|SEG_D|SEG_F|SEG_G,//S
	SEG_D|SEG_E|SEG_F|SEG_G, //t
	SEG_C|SEG_D|SEG_E, //u
	SEG_C|SEG_D|SEG_E, //v
	SEG_B|SEG_C|SEG_D|SEG_E|SEG_F, //W
	SEG_C|SEG_E|SEG_F|SEG_G, //X
	SEG_B|SEG_C|SEG_D|SEG_F|SEG_G, //Y
	SEG_A|SEG_B|SEG_D|SEG_E|SEG_G //Z
};

uint8_t modes[2][4] = {
	{
		SEG_B|SEG_C|SEG_D|SEG_E|SEG_G, //d
		SEG_B|SEG_C,//I
		SEG_A|SEG_C|SEG_D|SEG_F|SEG_G,//S
		0x00
	},
	{
		SEG_A|SEG_D|SEG_E|SEG_F, //C
		SEG_A|SEG_B|SEG_C|SEG_E|SEG_F|SEG_G, //A
		SEG_D|SEG_E|SEG_F,//L
		0x00 //blank
	}
};

int i = 0;
int j = 0;
char digits[4] = {
		0xFF,
		0xFF,
		0xFF,
		0xFF
};

char output[4] = {
		0xFF,
		0xFF,
		0xFF,
		0xFF
};

void d7seg_init(){
	P1DIR |= BIT0|BIT1|BIT2|BIT3;
//	P1OUT |= BIT0|BIT1|BIT2|BIT3;

	P3DIR = 0xFF;
	P3OUT = 0x00;

	Timer_A_initUpModeParam param = {0};
	param.clockSource = TIMER_A_CLOCKSOURCE_SMCLK;
	param.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_8;
	param.timerInterruptEnable_TAIE = TIMER_A_TAIE_INTERRUPT_DISABLE;
	param.captureCompareInterruptEnable_CCR0_CCIE = TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE;
	param.timerClear = TIMER_A_DO_CLEAR;
	param.startTimer = false;
	param.timerPeriod = 0xFF; //On period of 257 us. ~3.891kHz
	Timer_A_initUpMode(TIMER_A0_BASE, &param);

	Timer_A_startCounter(TIMER_A0_BASE, TIMER_A_UP_MODE);

	return;
}

void d7seg_printNum(int num){
	if(num > 9999) num = 9999;
	if(num < 0) num = 0;
	int k = 0;
	char temp[4] = {0};
	for(k = 0; k < 4; k++){
		temp[k] = num % 10;
		num = (num - temp[k]) / 10;
		digits[3-k] = numbers[temp[k]];
	}

	return;
}

void d7seg_printMode(int num){
	int k = 0;
	for(k = 0; k < 4; k++){
		digits[k] = modes[num][k];
	}

	return;
}

void d7seg_printStr(char* msg){
	int k = 0;
	for(k = 0; k < 4; k++){
		if(msg[k]=='\0'){
			digits[k] = 0x00;
		}else{
			digits[k] = alphabet[msg[k] - 'A'];
		}
	}
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer0_A0_ISR(void){
	P3OUT &= ~0xFF;
	P1OUT = (P1OUT & 0xF0) | 0x1 << i;
	P3OUT = output[i];
	i++;
	if(i > 3){
		i = 0;
	}
	j++;
	if(j > 391){
		output[0]=digits[0];
		output[1]=digits[1];
		output[2]=digits[2];
		output[3]=digits[3];
		j = 0;
	}
}

//TODO-remove once I find out why TAIE is being set
#pragma vector=TIMER0_A1_VECTOR
__interrupt void Timer0_A1_ISR(void){
	Timer_A_clearTimerInterrupt(TIMER_A0_BASE);
	//Its a trap!
}

