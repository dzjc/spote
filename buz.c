/*
 * buz.c
 *
 *  Created on: 16 Oct 2016
 *      Author: Dominic
 */
#include <msp430fr5739.h>
#include "driverlib/driverlib.h"
#include "buz.h"

int num = 0;

Timer_B_initUpModeParam param0 = {0};
Timer_B_initUpModeParam param1 = {0};

void buz_init(){
	P2DIR |= BIT6;
	P2SEL1 &= ~BIT6;
	P2SEL0 &= ~BIT6;
	P2OUT &= ~BIT6;

	//Buzzer drive timer
	param0.clockSource = TIMER_B_CLOCKSOURCE_SMCLK;
	param0.clockSourceDivider = TIMER_B_CLOCKSOURCE_DIVIDER_1;
	param0.timerPeriod = HIGHP;
	param0.timerInterruptEnable_TBIE = TIMER_B_TBIE_INTERRUPT_DISABLE;
	param0.captureCompareInterruptEnable_CCR0_CCIE = TIMER_B_CCIE_CCR0_INTERRUPT_ENABLE;
	param0.timerClear = TIMER_B_DO_CLEAR;
	param0.startTimer = false;
	Timer_B_initUpMode(TIMER_B0_BASE, &param0);

	//Buzzer on/off timer
	param1.clockSource = TIMER_B_CLOCKSOURCE_SMCLK;
	param1.clockSourceDivider = TIMER_B_CLOCKSOURCE_DIVIDER_64;
	param1.timerPeriod = 80000;
	param1.timerInterruptEnable_TBIE = TIMER_B_TBIE_INTERRUPT_DISABLE;
	param1.captureCompareInterruptEnable_CCR0_CCIE = TIMER_B_CCIE_CCR0_INTERRUPT_ENABLE;
	param1.timerClear = TIMER_B_DO_CLEAR;
	param1.startTimer = true;
	Timer_B_initUpMode(TIMER_B1_BASE, &param1);
}

void buz_buzbuz(int buzzes, int pitch){
	Timer_B_stop(TIMER_B0_BASE);
	Timer_B_stop(TIMER_B1_BASE);
	num = buzzes * 2;
	param0.timerPeriod = pitch;
	Timer_B_initUpMode(TIMER_B0_BASE, &param0);
	Timer_B_startCounter(TIMER_B1_BASE, TIMER_B_UP_MODE);
}
#pragma vector = TIMER0_B0_VECTOR
__interrupt void TIMER0_B0_ISR(void){
	P2OUT ^= BIT6;
}

#pragma vector = TIMER1_B0_VECTOR
__interrupt void TIMER1_B0_ISR(void){
	if(num == 0){
		Timer_B_stop(TIMER_B1_BASE);
		return;
	}
	num--;
	if(num % 2 == 0){
		Timer_B_stop(TIMER_B0_BASE);
	} else {
		Timer_B_startCounter(TIMER_B0_BASE, TIMER_B_UP_MODE);
	}
}
