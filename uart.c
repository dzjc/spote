/*
 * uart.c
 */

#include <msp430fr5739.h>
#include <stdint.h>
#include "driverlib/driverlib.h"

#define TXDPIN BIT4
#define RXDPIN BIT5

void uart_init(){
    //Pin config
    P2DIR |= TXDPIN | RXDPIN;	//Doesn't matter according to datasheet
    P2SEL1 |= TXDPIN | RXDPIN;	//Set pin functions to TXD and RXD
    P2SEL0 &= ~(TXDPIN | RXDPIN);
    //UART config
    EUSCI_A_UART_initParam config = {0};
    config.selectClockSource = EUSCI_A_UART_CLOCKSOURCE_SMCLK;
    config.clockPrescalar    = 8;
    config.firstModReg		 = 10;
    config.secondModReg		 = 0xF7;
    config.parity			 = EUSCI_A_UART_NO_PARITY;
    config.msborLsbFirst     = EUSCI_A_UART_LSB_FIRST;
    config.numberofStopBits  = EUSCI_A_UART_ONE_STOP_BIT;
    config.uartMode			 = EUSCI_A_UART_MODE;
    config.overSampling      = EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION;

    EUSCI_A_UART_init(EUSCI_A0_BASE, &config);
    EUSCI_A_UART_enable(EUSCI_A0_BASE);
}


void uart_print(char* msg){
	int i = 7;
    while( i != 0){
        EUSCI_A_UART_transmitData(EUSCI_A1_BASE, *msg);
        msg++;
        i--;
    }
}
