/*
 * adc.h
 *
 *  Created on: 29 Sep 2016
 *      Author: Dominic
 */

#ifndef ADC_H_
#define ADC_H_

void adc_setZero();
void adc_setScale();
int16_t adc_read();
void adc_init();

#endif /* ADC_H_ */
