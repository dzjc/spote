/*
 * main.c
 */

#include <msp430fr5739.h>
#include <intrinsics.h>
#include <stdint.h>

#include "driverlib/driverlib.h"
#include "adc.h"
#include "d7seg.h"
#include "uart.h"
#include "buz.h"

#define TXDPIN	BIT4			//Launchpad USB UART on PORT 3
#define RXDPIN	BIT5

//threshold variable for too high tension
#pragma PERSISTENT(thresh)
int16_t thresh = 2000;

int flag = 0;
int stage = 0;
int mode  = 0;

#define NUM_MODES 4

#define OFF 0
#define DIS 1
#define CAL	2
#define SET 3
#define TST 4

void init();
void proc_dis();
void proc_cal();
void proc_set();
void proc_tst();

int main(void) {    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    init();
    //uart_init();
    adc_init();
    d7seg_init();
    buz_init();
    mode = DIS;

    PM5CTL0 &= ~LOCKLPM5;		//Release pins from high-Z startup mode
    __enable_interrupt();
    
   //uart_print("init done!\r\n");
    __delay_cycles(100);
    while(1){
    	//main run loop
    	switch (mode){
    	case OFF:
    		d7seg_printStr("\0\0\0\0");
    		__low_power_mode_3();
    		break;
    	case DIS:
			proc_dis();
			break;
		case CAL:
			proc_cal();
			break;
		case SET:
			proc_set();
			break;
		case TST:
			proc_tst();
			break;
		default:
			while(1==1){
				d7seg_printStr("DED");
			}
    	}
    }


}

void init(){
	P1DIR &= ~(BIT4|BIT5|BIT6); //P1.4 1.5 1.6 inputs, buttons.
	P1OUT = (BIT4|BIT5|BIT6);
	P1REN = BIT4|BIT5|BIT6;

	P1IE |= BIT4|BIT5|BIT6;
	P1IES |= BIT4|BIT5|BIT6;
    //Set MCLK = 8MHz, SMCLK = 8MHz
    CS_setDCOFreq(CS_DCORSEL_0, CS_DCOFSEL_3);
    CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT,CS_CLOCK_DIVIDER_1);
    CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT,CS_CLOCK_DIVIDER_1);
}

void proc_dis(){
	if(stage == 0){
		//display current mode if we just entered and sleep until user input
		d7seg_printStr("DIS\0");
		__low_power_mode_0();
	}
	d7seg_printNum(adc_read());
}

void proc_cal(){
	//display current mode
	d7seg_printStr("CAL\0");
	//enter lpm0 while waiting for user input
	stage = 0;
	__low_power_mode_0();
	if(stage > 0){
		d7seg_printStr("ZERO");
		adc_setZero();
		__delay_cycles(4000000);
		d7seg_printStr("DONE");
		stage = 0;
		__low_power_mode_0();
	} else if(stage < 0){
		d7seg_printStr("FOUR");
		adc_setScale();
		__delay_cycles(4000000);
		d7seg_printStr("DONE");
		stage = 0;
		__low_power_mode_0();
	}
}

void proc_set(){
	//display current mode if we just entered and sleep until user input
	if(flag == 0){
		d7seg_printStr("SET\0");
		flag = 1;
		__low_power_mode_0();
		stage = 0;
	} else{
		//TODO - User Input feels a bit laggy here for some reason.
		if(stage == 1){
			thresh -= 10;
		} else if(stage == -1){
			thresh += 10;
		}
		stage = 0;
		d7seg_printNum(thresh);
	}
}

void proc_tst(){
	if(flag == 0){
		//display current mode if we just entered and sleep until user input
		d7seg_printStr("TST\0");
		flag = 1;
		__low_power_mode_0();
	}
	int value = adc_read();
	if(value > thresh){
		buz_buzbuz(3, HIGHP);
	}
	d7seg_printNum(value);
}

#pragma vector=PORT1_VECTOR
__interrupt void POR1_ISR(void){
	switch (P1IFG & (BIT4|BIT5|BIT6)){
	case BIT4:
		P1IFG &= ~BIT4;
		stage++;
		break;
	case BIT5:
		P1IFG &= ~BIT5;
		stage--;
		break;
	case BIT6:
		P1IFG &= ~BIT6;
		flag = 0;
		mode = (mode >= NUM_MODES)? 0: mode + 1;
		stage = 0;
		flag = 0;
		break;
	default:
		//welp just clear Interrupt Flag
		//don't support simultaneous button press...
		P1IFG = 0;
		break;
	}
	__low_power_mode_off_on_exit();
}

